DROP DATABASE IF EXISTS deals_database;

CREATE DATABASE deals_database;

USE deals_database;

CREATE TABLE IF NOT EXISTS instrument (
	instrument_id int NOT NULL PRIMARY KEY,
    instrument_name varchar(20) NOT NULL );

CREATE TABLE IF NOT EXISTS counter_party (
	cpty_id int NOT NULL PRIMARY KEY,
    cpty_name varchar(20) NOT NULL );
    
CREATE TABLE IF NOT EXISTS deal (
	deal_id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    instrument_id varchar(20),
    cpty_id varchar(20),
    price int,
    deal_type varchar(1) CHECK (deal_type in ("B", "S")),
    quantity int,
    deal_time int);
    
CREATE TABLE IF NOT EXISTS user_info (
	user_id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    user_name varchar(50) NOT NULL,
    user_email varchar(50) NOT NULL,
    user_password varchar(50) NOT NULL,
    user_role varchar(50) CHECK (user_role in ("Trader", "Senior Trader")));


SHOW TABLES;
