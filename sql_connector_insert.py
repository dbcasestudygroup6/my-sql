import mysql.connector
from mysql.connector import Error
from mysql.connector import errorcode

try:
   connection = mysql.connector.connect(host='192.168.99.100',
                             database='deals_database',
                             user='root',
                             password='ppp')

   sql_insert_query = """ INSERT INTO deal
                          (instrument, cpty, price, deal_type, quantity, deal_time) VALUES ('test_instrument','person','100', 'D', 50, 3)"""

   cursor = connection.cursor()
   result  = cursor.execute(sql_insert_query)
   connection.commit()
   print ("Record inserted successfully into deals_database table")

except mysql.connector.Error as error :
    connection.rollback() #rollback if any exception occured
    print("Failed inserting record into deals_database table {}".format(error))
finally:
    #closing database connection.
    if(connection.is_connected()):
        cursor.close()
        connection.close()
        print("MySQL connection is closed")