import mysql.connector
from mysql.connector import Error

try:
    connection = mysql.connector.connect(host='192.168.99.100',
                                         database='deals_database',
                                         user='root',
                                         password='ppp')
    sql_select_Query = "select * from deal"
    cursor = connection.cursor()
    cursor.execute(sql_select_Query)
    records = cursor.fetchall()
    print("Total number of rows in python_developers is - ", cursor.rowcount)
    print("Printing each row's column values i.e.  developer record")
    for row in records:
        print("Instrument = ", row[1], )
        print("Counterparty = ", row[2])
        print("Price  = ", row[3])
        print("Type = ", row[4])
        print("Quantity  = ", row[5])
        print("Time  = ", row[6], "\n")
    cursor.close()

except Error as e:
    print("Error while connecting to MySQL", e)
finally:
    # closing database connection.
    if (connection.is_connected()):
        connection.close()
        print("MySQL connection is closed")